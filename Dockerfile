# FROM node:15.13-alpine
# WORKDIR /myapp
# ENV PATH="./node_modules/.bin:$PATH"
# COPY . .
# RUN npm run build
# CMD ["npm", "start"]

FROM node:15.13-alpine
WORKDIR /myapp
COPY . .
RUN npm install
CMD ["npm", "start"]
EXPOSE 3000