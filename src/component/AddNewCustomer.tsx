import React, { useState, useEffect } from "react";

interface Customer {
  id: number;
  name: string;
  address?: string;
}

interface Props {
  listCustomersLenght: number;
  handleAddCustomer: (customer: Customer) => void;
  handleUpdateCustomer: (customer: Customer) => void;
  customerEdit?: Customer;
}

const AddNewCustomer: React.FC<Props> = (props) => {
  const [customerName, setcustomerName] = useState<string>();
  const [customerAddress, setcustomerAddress] = useState<string>();

  useEffect(() => {
    setcustomerName(props.customerEdit ? props.customerEdit.name : "");
    setcustomerAddress(
      props.customerEdit && props.customerEdit.address
        ? props.customerEdit.address
        : ""
    );
  }, [props.customerEdit]);

  const handleChangeNameValues = (name: string) => {
    setcustomerName(name);
  };
  const handleChangeAddressValues = (address: string) => {
    setcustomerAddress(address);
  };

  const handleSubmit = (e: React.MouseEvent) => {
    e.preventDefault();
    debugger;
    let customer: Customer = {
      id: props.listCustomersLenght + 1,
      name: customerName ? customerName : "",
      address: customerAddress ? customerAddress : "",
    };
    props.handleAddCustomer(customer);
  };

  const handleUpdate = (e: React.MouseEvent) => {
    e.preventDefault();
    debugger;
    let customer: Customer = {
      id: props.customerEdit ? props.customerEdit.id : -1,
      name: customerName ? customerName : "",
      address: customerAddress ? customerAddress : "",
    };
    props.handleUpdateCustomer(customer);
  };

  const handleClear = (e: React.MouseEvent) => {
    e.preventDefault();
    setcustomerName("");
    setcustomerAddress("");
  };

  return (
    <>
      <form>
        <div>
          <span style={{ marginRight: "10px" }}>name</span>
          <input
            type="text"
            value={customerName}
            onChange={(e) => handleChangeNameValues(e.target.value)}
          />
        </div>
        <div>
          <span style={{ marginRight: "10px" }}>address</span>
          <input
            type="text"
            value={customerAddress}
            onChange={(e) => handleChangeAddressValues(e.target.value)}
          />
        </div>
        <button onClick={(e) => handleSubmit(e)}>add</button>
        <button onClick={(e) => handleUpdate(e)}>update</button>
        <button onClick={(e) => handleClear(e)}>clear</button>
      </form>
    </>
  );
};

export default AddNewCustomer;
